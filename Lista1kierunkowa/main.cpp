#include <iostream>
#include <string>
//#include <cstdlib>

using namespace std;

class student {
	public:
		int index;
	    string name;
	    string surname;
	    int yearOfStudy;
	    string fieldOfStudy;
	    string specialty;
	    student *next; //wskaznik na nastepny
	    student();
};

student::student() {
    next = 0;
}

class llist {
	public:
	    void insertAtEnd (int index, string name, string surname, int yearOfStudy, string fieldOfStudy, string specialty);
	    void insertAtBeginning (int index, string name, string surname, int yearOfStudy, string fieldOfStudy, string specialty);
	    void insertAtGivenPosition(int a, string n, int index, string name, string surname, int yearOfStudy, string fieldOfStudy, string specialty);
	    void findStudent(int index, string surname);
	    void deleteLast ();
	    void deleteSelected(int index, string surname);
	    void deleteAll ();
	    void displayList ();
	    student *first; //wskaznik na pierwszy
	    llist();
};

llist::llist() {
    first = 0; //konstruktor
}

void llist::insertAtBeginning(int index, string name, string surname, int yearOfStudy, string fieldOfStudy, string specialty){
	student *newStudent = new student; //tworzenie nowego obiektu
	newStudent->index = index;
	newStudent->name = name;
	newStudent->surname = surname;
	newStudent->yearOfStudy = yearOfStudy;
	newStudent->fieldOfStudy = fieldOfStudy;
    newStudent->specialty = specialty;

	if (first != 0) // jesli nie jest pierwszy
		newStudent->next = first;//nastepny z nowego elementu wskazuje na dotychczasowy pierwszy
	first = newStudent;// pierwszym jest teraz newStudent
}

void llist::insertAtEnd(int index, string name, string surname, int yearOfStudy, string fieldOfStudy, string specialty){
	student *newStudent = new student; //tworzenie nowego obiektu
	newStudent->index = index;
	newStudent->name = name;
	newStudent->surname = surname;
	newStudent->yearOfStudy = yearOfStudy;
	newStudent->fieldOfStudy = fieldOfStudy;
	newStudent->specialty = specialty;

	if(first==0){ //jezeli nie ma pierwszego
		first = newStudent; //newStudent jest pierwszy
	}
	else{ // jesli jest pierwszy
		student *tmp = first;  //tymczasowy wskazuje na pierwszy
		while(tmp->next){ //szukanie ostatniego elementu
			tmp = tmp->next; //przesuwamy tmp na nastepny element
		}
		tmp->next=newStudent; //newStudent jest teraz next tmp
		newStudent->next=0; //bo nie bedzie nastepnego
	}
}

void llist::insertAtGivenPosition(int a, string n, int index, string name, string surname, int yearOfStudy, string fieldOfStudy, string specialty) {
	student *newStudent = new student; // tworzenie nowego obiektu
	student *tmp = first; //tymczasowy wskazuje na pierwszy
	newStudent->index = index;
	newStudent->name = name;
	newStudent->surname = surname;
	newStudent->yearOfStudy = yearOfStudy;
	newStudent->fieldOfStudy = fieldOfStudy;
	newStudent->specialty = specialty;
	if (first == 0) { // jesli nie ma pierwszego
		first = newStudent; // newStudent jest pierwszy
	}
	else { // w przeciwnym wypadku
		while (tmp) { // przeszukiwanie listy
			index = a;
			surname = n;
			if (tmp->index == index && tmp->surname == surname) { //jesli argumenty z metody zgadzaja sie z tymi z obiektu
				newStudent->next = tmp->next; //wskaznik na nastepny element elementu newStudent jest rowny wsk na nastepny element, elementu tmp
				tmp->next = newStudent; //wskaznik nastepnego elementu tmp, wskazuje na element wstawiony - newStudent
			}
        tmp = tmp->next; //przesuwamy tmp na nastepny element
        }
    }
}

void llist::findStudent(int index, string surname) {
	student *newStudent = new student; //tworzymy nowy obiekt
	student *tmp = first; //tymczasowy wskazuje na pierwszy
	while (tmp) { // przeszukiwanie listy
		if (tmp->index == index && tmp->surname == surname) { //jesli argumenty z metody zgadzaja sie z tymi z obiektu
			cout << "nr indexu: " << index << endl;
			cout << "name: " << tmp->name << endl;
			cout << "surname: " << surname << endl;
			cout << "years of study: " << tmp->yearOfStudy << endl;
			cout << "field of study: " << tmp->fieldOfStudy << endl;
			cout << "specialty: " << tmp->specialty << endl << endl;
		}
		tmp = tmp->next; // przesuwamy tmp na nastepny element
	}
}

void llist::deleteLast(){
	if (first->next == 0){ //jesli jest tylko jeden
		delete first; //usuwany ten element
		first = 0; //nie ma nic
	}
	else{
		student *previous = first;  // tymczasowy 'poprzedni' wskazujacy na pierwszy
		student *last = first->next; // tymczasowy 'ostatni' ktory jest nastepnym z pierwszego
		while (last->next != 0){ //dopoki nastepny z ostatniego jest rozny od 0
			previous = last; // poprzedni wskazuje na dotychczasowy ostatni
			last = last->next; //przesuwamy ostatni na next element
		}
		delete last; //usuwamy ostatni
		previous->next = 0; //bo nie ma bedzie kolejnego
	}
	cout << "The last element from the list was deleted.\n"; //ostatni element z listy zostal usuniety
}

void llist::deleteSelected(int index, string surname) {
	student *tmp = first; //tymczasowy wskazuje na pierwszy
	student *previous = first; //poprzedni wskazuje na pierwszy
	while (tmp != 0) { //dopoki lista nie jest pusta
		if (tmp->index == index && tmp->surname == surname) { //jesli argumenty z metody zgadzaja sie z tymi z obiektu
			if (tmp == first) { //jesli jest pierwszy elementem
				first = first->next; //pierwszy staje sie nastepnym z dotychczasowego pierwszego
				delete tmp; //usuwamy tmp
				tmp = first; //tmp jest nowym pierwszym
				previous = first; //poprzedni jest nowym pierwszym
			}
			else { //jesli nie jest pierwszym
				previous->next = tmp->next; // next z poprzedniego wskazuje na next z tmp
				delete tmp; //deleting tmp
				tmp = previous->next; //tmp jest teraz next z poprzedniego
			}
		}
		previous = tmp; //poprzedni jest tmp
		tmp = tmp->next; //przesuwamy tmp do nastepnego
	}
}


void llist::deleteAll(){
	student *tmp = first; //tymczasowy wskazuje na pierwszy

	while (tmp != 0){ //dopoki lista nie jest pusta
		first = first->next; //pierwszy jest teraz next z owczsnego pierwszego
		delete tmp; //usuwamy tmp
		tmp = first; // tmp jest pierwszym
	}
	cout << "The list was deleted.\n";
}

void llist::displayList(){
	student *tmp = first; // tymczasowy wskazuje na pierwszy
	if (first == 0) cout << "There are no students on the list." << endl;
	int i = 1;
	while (tmp) { //dopoki cos na liscie
		cout << "STUDENT NR " << i << ": " << endl;
		cout << "Index: " << tmp->index << endl;
		cout << "name: " << tmp->name << endl;
		cout << "surname: " << tmp->surname << endl;
		cout << "years of study: " << tmp->yearOfStudy << endl;
		cout << "field of study: " << tmp->fieldOfStudy << endl;
		cout << "specialty: " << tmp->specialty << endl << endl;
		tmp = tmp->next; // przesuwamy tmp na nastepny
		i++;
	}
}

void menu() {
	cout << "MENU " << endl;
	cout << "1. Insert element at the beginning.\n";
	cout << "2. Insert element at the end\n";
	cout << "3. Insert at the selected position\n";
	cout << "4. Display list\n";
	cout << "5. Find student\n";
	cout << "6. Delete last element on the list\n";
	cout << "7. Delete selected element\n";
	cout << "8. Delete all elements\n";
	cout << "9. Display menu\n";
	cout << "0. End of program\n";
}

int main() {
	llist *students = new llist;
	string name, surname, fieldOfStudy, specialty, n;
	int index, yearOfStudy, a;
	menu();
	int choice;
	do
	{
		cout << "\nChoices: (9 - menu) ";
		cin >> choice;
		switch (choice)
		{
		case 1:
			cout << "Enter the student's index: ";
			cin >> index;
			cout << "Enter the student's name: ";
			cin >> name;
			cout << "Enter the student's surname: ";
			cin >> surname;
			cout << "Enter the student's years of study: ";
			cin >> yearOfStudy;
			cout << "Enter the student's field of study: ";
			cin >> fieldOfStudy;
			cout << "Enter the student's specialty: ";
			cin >> specialty;
			students->insertAtBeginning(index, name, surname, yearOfStudy, fieldOfStudy, specialty);
			break;
		case 2:
			cout << "Enter the student's index: ";
			cin >> index;
			cout << "Enter the student's name: ";
			cin >> name;
			cout << "Enter the student's surname: ";
			cin >> surname;
			cout << "Enter the student's years of study: ";
			cin >> yearOfStudy;
			cout << "Enter the student's field of study: ";
			cin >> fieldOfStudy;
			cout << "Enter the student's specialty: ";
			cin >> specialty;
			students->insertAtEnd(index, name, surname, yearOfStudy, fieldOfStudy, specialty);
			break;
		case 3:
			cout << "Enter the student's index number for which you want to insert another ";
			cin >> a;
			cout << "Enter his surname: ";
			cin >> n;
			cout << "Enter the student's index: ";
            cin >> index;
			cout << "Enter the student's name: ";
			cin >> name;
			cout << "Enter the student's surname: ";
			cin >> surname;
			cout << "Enter the student's years of study: ";
			cin >> yearOfStudy;
			cout << "Enter the student's field of study: ";
			cin >> fieldOfStudy;
			cout << "Enter the student's specialty: ";
			cin >> specialty;
			students->insertAtGivenPosition(a, n, index, name, surname, yearOfStudy, fieldOfStudy, specialty);
			break;
		case 4:
			students->displayList();
			break;
		case 5:
			cout << "Enter the student's index that you are looking for: ";
			cin >> a;
			cout << "Enter his surname: ";
			cin >> n;
			cout << endl << "Info about the student that was found: " << endl;
			students->findStudent(a, n);
			break;

		case 6:
			students->deleteLast();
			break;
		case 7:
			cout << "Enter the student's index that you want to delete: ";
			cin >> a;
			cout << "Enter his surname: ";
			cin >> n;
			students->deleteSelected(a, n);
			cout << "Deleted: " << a << " " << n << endl;
			break;
		case 8:
			students->deleteAll();
			break;
		case 9:
			menu();
			break;
		case 0:
			cout << endl << "End of program." << endl;
			break;
		default:
			cout << "Wrong character, try one more time " << endl;
		}
	} while (choice != 0);

	delete(students);     //usuwamy liste

	return 0;
}
